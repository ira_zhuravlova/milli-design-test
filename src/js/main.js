require('../scss/app.scss');
require('../scss/swiper.min.css');
require('bootstrap/dist/css/bootstrap.css');

var $ = require('jquery'),
    Swiper = require('./utils/swiperUtil'),
    modal = $('#myModal');

Swiper();

$('#myBtn').on('click', function(e) {
    e.preventDefault();
    $(modal).fadeIn('slow');
});

$('.modal__close').on('click', function(e) {
    $(modal).fadeOut('slow');
});

$('#myModal').on('click', function(e) {
    if (e.target.id === 'myModal') {
        $(modal).fadeOut('slow');
    }

});

$('.nav__menu-button').on('click', function(e) {
    $('#menu-list').slideToggle('slow');
});