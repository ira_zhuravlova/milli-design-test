var Swiper = require('swiper');

var activateSwiper = function(){
    var mySwiper = new Swiper ('.swiper-container', {
        loop: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        autoplay: 5000
    });
    
};


module.exports = activateSwiper;
